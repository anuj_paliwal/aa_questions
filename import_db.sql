CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(30) NOT NULL,
  lname VARCHAR(30) NOT NULL
);

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(200),
  body TEXT(500),
  user_id INTEGER,
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE questions_followers (
  user_id INTEGER,
  question_id INTEGER,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  question_id INTEGER,
  reply_id INTEGER,
  user_id INTEGER,
  body TEXT(500),
  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (reply_id) REFERENCES replies(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE questions_likes (
  id INTEGER PRIMARY KEY,
  user_id INTEGER,
  question_id INTEGER,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);


INSERT INTO
  users (fname, lname)
VALUES
  ('john', 'smith'), ('jack', 'black');


INSERT INTO
  questions (title, body, user_id)
VALUES
  ('Movies', 'What is the best movie ever?', 1);

INSERT INTO
  questions (title, body, user_id)
VALUES
  ('The Second Title', 'Even better than the first title', 1);


INSERT INTO
  questions (title, body, user_id)
VALUES
  ('Dont Cry For Me Argentina', 'The truth is I never left you', 2);

INSERT INTO
  replies (question_id, reply_id, user_id, body)
VALUES
  (1, null, 1, "Obvi, Matrix!");

INSERT INTO
  replies (question_id, reply_id, user_id, body)
VALUES
  (1, 1, 2, "No way");


INSERT INTO
  questions_followers (user_id, question_id)
VALUES
  (1, 1), (2, 1);

INSERT INTO
  questions_likes (user_id, question_id)
VALUES
  (1, 1), (1, 2), (2, 1);


