require 'singleton'
require 'sqlite3'

class QuestionsDatabase < SQLite3::Database

  include Singleton

  def initialize
    super("questions.db")

    self.results_as_hash = true
    self.type_translation = true
  end
end

class User
  def self.find_by_name(fname, lname)
    results = QuestionsDatabase.instance.execute(<<-SQL, fname, lname)
      SELECT
        *
      FROM
        users
      WHERE
        fname = ? AND lname = ?
    SQL

    results.map { |result| User.new(result) }
  end

  attr_accessor :id, :fname, :lname

  def initialize(options = {})
    @id = options["id"]
    @fname = options["fname"]
    @lname = options["lname"]
  end

  def create
    QuestionsDatabase.instance.execute(<<-SQL, fname, lname)
      INSERT INTO
        users (fname, lname)
      VALUES
        (?, ?)
    SQL

    @id = QuestionsDatabase.instance.last_insert_row_id
  end

  def authored_questions
    Question.find_by_author_id(self.id)
  end

  def authored_replies
    Reply.find_by_author_id(self.id)
  end

  def followed_questions
    QuestionFollower.followed_questions_user_id(self.id)
  end

  def liked_questions
    QuestionLike.liked_questions_for_user_id(self.id)
  end

  def average_karma
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => self.id)

    SELECT q.user_id, q.question_id
    FROM
    questions_likes q
    JOIN
    questions ON
    (questions.id = q.question_id)
    WHERE
    questions.user_id = :id

    SQL

    results
  end

end

class Question
  def self.find_by_author_id(user)
  results = QuestionsDatabase.instance.execute(<<-SQL, :user => user)
    SELECT
      *
    FROM
      questions
    WHERE
      questions.user_id = :user
  SQL

  results.map { |result| Question.new(result) }
  end

  def self.most_followed(n)
    QuestionFollower.most_followed_questions(n)
  end

  def self.most_liked(n)
    QuestionLike.most_liked(n)
  end

  attr_accessor :author, :id, :body, :user_id

  def initialize(options = {})
    @id = options["id"]
    @title = options["title"]
    @body = options["body"]
    @user_id = options["user_id"]
  end

  def create
    QuestionsDatabase.instance.execute(<<-SQL, title, body, user_id)
      INSERT INTO
        questions (title, body, user_id)
      VALUES
        (?, ?, ?)
    SQL

    @id = QuestionsDatabase.instance.last_insert_row_id
  end

  def author
    results = QuestionsDatabase.instance.execute(<<-SQL, :question_id => self.id)
      SELECT users.fname, users.lname
      FROM users
      WHERE users.id = (SELECT questions.user_id
      FROM questions
      WHERE questions.user_id = users.id
        AND questions.id = :question_id)
    SQL

    results.map { |result| User.new(result) }
  end

  def replies
    Reply.find_by_question_id(self.id)
  end

  def followers
    QuestionFollower.followers_for_question_id(self.id)
  end

  def likers
    QuestionLike.likers_for_question_id(self.id)
  end

  def num_likes
    QuestionLike.num_likes_for_question_id(self.id)
  end

end

class Reply

  attr_accessor :id, :question_id, :reply_id, :user_id, :body

  def initialize(options = {})
    @id = options["id"]
    @question_id = options["question_id"]
    @reply_id = options["reply_id"] # does this need to be initialized as nil?
    @user_id = options["user_id"]
    @body = options["body"]
  end

  def self.find_by_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :user_id => user_id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.user_id = :user_id
    SQL

    results.map { |result| Reply.new(result) }
  end

  def self.find_by_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :question_id => question_id)
      SELECT
        *
      FROM
        replies
      WHERE
        replies.question_id = :question_id
    SQL

    results.map { |result| Reply.new(result) }
  end

  def author
    results = QuestionsDatabase.instance.execute(<<-SQL, :reply_id => self.id)
      SELECT
        fname, lname
      FROM
        users
      WHERE
        users.id = (SELECT user_id
      FROM
        replies
      WHERE
        users.id = replies.user_id
      AND
        replies.id = :reply_id)
    SQL

    results.map {|result| User.new(result)}
  end

  def question
    Reply.find_by_question_id(self.question_id)
  end

  def parent_reply
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => self.id)
      SELECT
        body
      FROM
        replies r1
      WHERE
        r1.id =
      (SELECT
        r2.reply_id
      FROM
        replies r2
      WHERE
        :id = r2.id)
    SQL

    results.map { |result| Reply.new(result) }
  end

  def child_replies
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => self.id)
      SELECT
        body
      FROM
        replies r1
      WHERE
        r1.reply_id =
      (SELECT
        r2.id
      FROM
        replies r2
      WHERE
        :id = r2.id)
    SQL

    results.map { |result| Reply.new(result) }
  end



end

class QuestionFollower

  def self.followers_for_question(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => question_id)
      SELECT
        fname, lname
      FROM
        users
      JOIN
        questions_followers q
      ON (users.id = q.user_id)
      WHERE
      :id = q.question_id
    SQL

    results.map { |result| User.new(result) }
  end

  def self.followed_questions_for_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => user_id)
    SELECT
      title, body
    FROM
      questions
    JOIN
      questions_followers AS q
    ON
      (q.question_id = questions.id)
    WHERE
      :id = q.user_id
    SQL

    results.map { |result| Question.new(result) }
  end

  def self.most_followed_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL, :n => n)
      SELECT title, body, COUNT(*)
      FROM
      questions
      JOIN
      questions_followers q ON (questions.id = q.question_id)
      GROUP BY questions.id
      ORDER BY COUNT(*)
      LIMIT :n
    SQL
  end

end

class QuestionLike

  def self.likers_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => question_id)
      SELECT
        fname, lname
      FROM
        users
      JOIN
        questions_likes q ON (users.id = q.user_id)
      WHERE
        q.user_id = users.id
      AND q.question_id = :id
    SQL

    results.map {|result| User.new(result)}
  end

  def self.num_likes_for_question_id(question_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => question_id)
      SELECT
        COUNT(q.user_id)
      FROM
        questions_likes q
      WHERE
        :id = q.question_id
      GROUP BY
        q.question_id
    SQL
  end

  def self.liked_questions_for_user_id(user_id)
    results = QuestionsDatabase.instance.execute(<<-SQL, :id => user_id)
      SELECT
        body, title
      FROM
        questions_likes q
      JOIN
        questions
      ON
        (q.question_id = questions.id)
      WHERE
        q.user_id = :id
      SQL

    results.map { |result| Question.new(result) }
  end

  def self.most_liked_questions(n)
    results = QuestionsDatabase.instance.execute(<<-SQL, :n => n)
    SELECT
      title, body, COUNT(*) num_likes
    FROM
      questions_likes q
    JOIN
      questions
    ON
      q.question_id = questions.id
    GROUP BY q.question_id
    ORDER BY COUNT(*) DESC
    LIMIT :n
    SQL
  end

  def self.most_liked(n)
  end


end
